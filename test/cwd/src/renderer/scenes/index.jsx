import {
  DOWNLOADS,
  INSIGHTS,
  INSIGHTS_ORG,
  INSIGHTS_ORG_PLANS,
  INSIGHTS_ORG_PLANS_DETAILS,
  INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE,
  INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_EDIT,
  INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_NEW,
  INSIGHTS_ORG_SCHEDULE,
  INSIGHTS_ORG_SCHEDULE_EDIT,
  INSIGHTS_ORG_SCHEDULE_NEW,
  LIBRARY,
  LOGIN,
  LOGIN_REDIRECT,
  MANUAL_TASKS,
  MANUAL_TASKS_ORG,
  MANUAL_TASKS_ENTRY,
  ORGANIZATIONS,
  ORGANIZATIONS_EDIT,
  ORGANIZATIONS_NEW,
  ORGANIZATIONS_OVERVIEW,
  PAGE_NOT_FOUND,
  PLANS,
  PLANS_ORG,
  PLAN_EDIT,
  PLAN_HISTORY,
  PLAN_NEW,
  PLAN_START_RUN,
  PLAN_RUN,
  PUBLIC_AUTOMATIONS,
  PUBLIC_AUTOMATIONS_DETAILS,
  PUBLIC_AUTOMATIONS_EDIT,
  PUBLIC_AUTOMATIONS_OVERVIEW,
  PUBLIC_AUTOMATIONS_NEW,
  PUBLIC_INSIGHTS,
  PUBLIC_INSIGHTS_ORG,
  PUBLIC_MANUAL_TASKS,
  PUBLIC_MANUAL_TASKS_ORG,
  PUBLIC_MANUAL_TASKS_ENTRY,
  PUBLIC_WORKSTATIONS,
  PUBLIC_WORKSTATIONS_NEW,
  SECRETS,
  SECRETS_EDIT,
  SECRETS_NEW,
  SHARED_INSIGHTS,
  SKILLS,
  SKILLS_EDIT,
  SKILLS_NEW,
  WORKFLOWS,
  WORKSTATIONS,
  WORKSTATIONS_ORG,
  WORKSTATION_EDIT,
  WORKSTATION_NEW,
} from 'redux-modules/router/constants';

import Downloads from './downloads';
import Library from './library';
import Login from './login';
import LoginRedirect from './login/loginRedirect';
import Organizations from './organizations';
import OrgPlans from './plans/orgPlans';
import PageNotFound from './pageNotFound';
import Plans from './plans';
import PlanForm from './plans/newEditPlan';
import PlanHistory from './plans/history';
import PlanLive from './plans/live';
import PlanStartRun from './plans/startRun';
import Secrets from './secrets';
import Skills from './skills';
import Workflows from './workflows';
import Workstations from './workstations';

import PublicAutomations from './public/automations';
import PublicInsights from './public/insights';
import PublicTasks from './public/tasks';
import PublicWorkstations from './public/workstations';

export default {
  [DOWNLOADS]: Downloads,
  [INSIGHTS]: PublicInsights,
  [INSIGHTS_ORG]: PublicInsights,
  [INSIGHTS_ORG_PLANS]: PublicInsights,
  [INSIGHTS_ORG_PLANS_DETAILS]: PublicInsights,
  [INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE]: PublicInsights,
  [INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_EDIT]: PublicInsights,
  [INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_NEW]: PublicInsights,
  [INSIGHTS_ORG_SCHEDULE]: PublicInsights,
  [INSIGHTS_ORG_SCHEDULE_EDIT]: PublicInsights,
  [INSIGHTS_ORG_SCHEDULE_NEW]: PublicInsights,
  [LIBRARY]: Library,
  [LOGIN]: Login,
  [LOGIN_REDIRECT]: LoginRedirect,
  [MANUAL_TASKS]: PublicTasks,
  [MANUAL_TASKS_ORG]: PublicTasks,
  [MANUAL_TASKS_ENTRY]: PublicTasks,
  [ORGANIZATIONS]: Organizations,
  [ORGANIZATIONS_EDIT]: Organizations,
  [ORGANIZATIONS_NEW]: Organizations,
  [ORGANIZATIONS_OVERVIEW]: Organizations,
  [PAGE_NOT_FOUND]: PageNotFound,
  [PLANS]: Plans,
  [PLANS_ORG]: OrgPlans,
  [PLAN_EDIT]: PlanForm,
  [PLAN_HISTORY]: PlanHistory,
  [PLAN_NEW]: PlanForm,
  [PLAN_START_RUN]: PlanStartRun,
  [PLAN_RUN]: PlanLive,
  [PUBLIC_AUTOMATIONS]: PublicAutomations,
  [PUBLIC_AUTOMATIONS_DETAILS]: PublicAutomations,
  [PUBLIC_AUTOMATIONS_EDIT]: PublicAutomations,
  [PUBLIC_AUTOMATIONS_OVERVIEW]: PublicAutomations,
  [PUBLIC_AUTOMATIONS_NEW]: PublicAutomations,
  [PUBLIC_INSIGHTS]: PublicInsights,
  [PUBLIC_INSIGHTS_ORG]: PublicInsights,
  [PUBLIC_MANUAL_TASKS]: PublicTasks,
  [PUBLIC_MANUAL_TASKS_ORG]: PublicTasks,
  [PUBLIC_MANUAL_TASKS_ENTRY]: PublicTasks,
  [PUBLIC_WORKSTATIONS]: PublicWorkstations,
  [PUBLIC_WORKSTATIONS_NEW]: PublicWorkstations,
  [SECRETS]: Secrets,
  [SECRETS_EDIT]: Secrets,
  [SECRETS_NEW]: Secrets,
  [SHARED_INSIGHTS]: PublicInsights,
  [SKILLS]: Skills,
  [SKILLS_EDIT]: Skills,
  [SKILLS_NEW]: Skills,
  [WORKFLOWS]: Workflows,
  [WORKSTATIONS]: Workstations,
  [WORKSTATIONS_ORG]: Workstations,
  [WORKSTATION_EDIT]: Workstations,
  [WORKSTATION_NEW]: Workstations,
};
