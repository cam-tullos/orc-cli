export const DOWNLOADS = 'Downloads';
export const INSIGHTS = 'Insights';
export const INSIGHTS_ORG = 'Insights.Organization';
export const INSIGHTS_ORG_PLANS = 'Insights.Organization.Plans';
export const INSIGHTS_ORG_PLANS_DETAILS = 'Insights.Organization.Plans.Details';
export const INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE = 'Insights.Organization.Plans.Details.Schedule';
export const INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_EDIT = 'Insights.Organization.Plans.Details.Schedule.Edit';
export const INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_NEW = 'Insights.Organization.Plans.Details.Schedule.New';
export const INSIGHTS_ORG_SCHEDULE = 'Insights.Organization.Schedule';
export const INSIGHTS_ORG_SCHEDULE_EDIT = 'Insights.Organization.Schedule.Edit';
export const INSIGHTS_ORG_SCHEDULE_NEW = 'Insights.Organization.Schedule.New';
export const LIBRARY = 'Library';
export const LOGIN = 'Login';
export const LOGIN_REDIRECT = 'Login.Redirect';
export const MANUAL_TASKS = 'Human Intervention';
export const MANUAL_TASKS_ORG = 'Human Intervention.Organization';
export const MANUAL_TASKS_ENTRY = 'Human Intervention.Entry';
export const ORGANIZATIONS = 'Organizations';
export const ORGANIZATIONS_EDIT = 'Organizations.Edit';
export const ORGANIZATIONS_NEW = 'Organizations.New';
export const ORGANIZATIONS_OVERVIEW = 'Organizations.Overview';
export const PAGE_NOT_FOUND = 'PageNotFound';
export const PLANS = 'Plans';
export const PLANS_ORG = 'Plans.Organization';
export const PLAN_EDIT = 'Plans.Edit';
export const PLAN_NEW = 'Plans.New';
export const PLAN_HISTORY = 'Plans.History';
export const PLAN_START_RUN = 'Plans.StartRun';
export const PLAN_RUN = 'Plans.Run';
export const PUBLIC = 'Public';
export const PUBLIC_AUTOMATIONS = 'Automations';
export const PUBLIC_AUTOMATIONS_DETAILS = 'Automations.Details';
export const PUBLIC_AUTOMATIONS_EDIT = 'Automations.Edit';
export const PUBLIC_AUTOMATIONS_OVERVIEW = 'Automations.Overview';
export const PUBLIC_AUTOMATIONS_NEW = 'Automations.New';
export const PUBLIC_INSIGHTS = 'Public.Insights';
export const PUBLIC_INSIGHTS_ORG = 'Public.Insights.Organization';
export const PUBLIC_MANUAL_TASKS = 'Public.Human Intervention';
export const PUBLIC_MANUAL_TASKS_ORG = 'Public.Human Intervention.Organization';
export const PUBLIC_MANUAL_TASKS_ENTRY = 'Public.Human Intervention.Entry';
export const PUBLIC_WORKSTATIONS = 'Public.Workstations';
export const PUBLIC_WORKSTATIONS_NEW = 'Public.Workstations.New';
export const SECRETS = 'Secrets';
export const SECRETS_EDIT = 'Secrets.Edit';
export const SECRETS_NEW = 'Secrets.New';
export const SHARED = 'Shared';
export const SHARED_INSIGHTS = 'Shared.Insights';
export const SKILLS = 'Skills';
export const SKILLS_EDIT = 'Skills.Edit';
export const SKILLS_NEW = 'Skills.New';
export const WORKFLOWS = 'Workflows';
export const WORKSTATIONS = 'Workstations';
export const WORKSTATIONS_ORG = 'Workstations.Organization';
export const WORKSTATION_EDIT = 'Workstations.Edit';
export const WORKSTATION_NEW = 'Workstations.New';

export const SHEET = 'bottomSheet';
export const TEST_BOTTOM_SHEET = 'TEST_BOTTOM_SHEET';


// Add your constants here if you want the default state for panelErrors & drawer states.
export default {
  DOWNLOADS,
  INSIGHTS,
  INSIGHTS_ORG,
  INSIGHTS_ORG_PLANS,
  INSIGHTS_ORG_PLANS_DETAILS,
  INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE,
  INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_EDIT,
  INSIGHTS_ORG_PLANS_DETAILS_SCHEDULE_NEW,
  INSIGHTS_ORG_SCHEDULE,
  INSIGHTS_ORG_SCHEDULE_EDIT,
  INSIGHTS_ORG_SCHEDULE_NEW,
  LIBRARY,
  LOGIN,
  LOGIN_REDIRECT,
  MANUAL_TASKS,
  MANUAL_TASKS_ORG,
  MANUAL_TASKS_ENTRY,
  ORGANIZATIONS,
  ORGANIZATIONS_EDIT,
  ORGANIZATIONS_NEW,
  ORGANIZATIONS_OVERVIEW,
  PAGE_NOT_FOUND,
  PLANS,
  PLANS_ORG,
  PLAN_EDIT,
  PLAN_NEW,
  PLAN_HISTORY,
  PLAN_START_RUN,
  PLAN_RUN,
  PUBLIC_AUTOMATIONS,
  PUBLIC_AUTOMATIONS_DETAILS,
  PUBLIC_AUTOMATIONS_EDIT,
  PUBLIC_AUTOMATIONS_OVERVIEW,
  PUBLIC_AUTOMATIONS_NEW,
  PUBLIC_INSIGHTS,
  PUBLIC_INSIGHTS_ORG,
  PUBLIC_MANUAL_TASKS,
  PUBLIC_MANUAL_TASKS_ORG,
  PUBLIC_MANUAL_TASKS_ENTRY,
  PUBLIC_WORKSTATIONS,
  PUBLIC_WORKSTATIONS_NEW,
  SECRETS,
  SECRETS_EDIT,
  SECRETS_NEW,
  SKILLS,
  SKILLS_EDIT,
  SKILLS_NEW,
  WORKFLOWS,
  WORKSTATIONS,
  WORKSTATION_NEW,
  WORKSTATION_EDIT,
  WORKSTATIONS_ORG,
};
