
const sceneGen      = require('../commands/scene/generator');
const config        = require("../config.json");
const beautify      = require('js-beautify');
const path          = require('path');
const chai          = require('chai');

const should        = chai.should();
const assert        = chai.assert;
const expect        = chai.expect;


describe('Create Scene', function() {
    const props =  {
        cwd  : path.normalize(path.join(__dirname, 'cwd')),
        root : path.normalize(path.join(__dirname, '..')),
        config,
    };

    const params = {
        "id"            : "Admin",
        "constant"      : "ADMIN_SCENE",
        "constantValue" : "Admin",
        "route"         : "/admin/:something",
        "icon"          : "settings",
        "path"          : "/admin"
    };

    const q = sceneGen({ params, props });

    it ('All actions complete', () => {
        expect(q.result).to.have.lengthOf(4);
        console.log('\n    '+q.errors.join('\n    ')+'\n');
    });
});
