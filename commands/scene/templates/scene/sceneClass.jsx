import React from 'react';
import Flexbox from 'flexbox-react';
import SceneHeader from 'containers/sceneHeader';

export default class [ID] extends React.Component {
  render() {
    return (
      <Flexbox
        alignItems="center"
        className=""
        flexDirection="column"
      >
        <SceneHeader
          breadcrumb={[(
            <span key="header" className="breadcrumb -no-link">
              [ID]
            </span>
          )]}
          icon="[ICON]"
        />
      </Flexbox>
    );
  }
}
