import { connect } from 'react-redux';
import [ID] from './[DIR]Class';
import './[DIR].scss';


const mapStateToProps = state => ({
  ...state,
});

const actions = {
};

export default connect(
  mapStateToProps,
  actions,
)([ID]);
