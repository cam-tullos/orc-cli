const fs = require('fs-extra');
const path = require('path');
const op = require('object-path');


const actions = {
    component: ({ params, props, test = false }) => {
        let { id, icon = '', path:componentPath, overwrite = false } = params;

        let testOutput = {};

        // Set the destination directory
        const dirName = (componentPath)
            ? (componentPath.substr(0, 1) === '/')
                ? componentPath.substr(1)
                : componentPath
            : String(id).toLowerCase();

        // Get the template path
        const templatePath = path.normalize(path.join(
            props.root, 'commands', 'scene', 'templates', 'scene'
        ));

        // Get the destination path
        const appPath = path.normalize(path.join(
            props.cwd, 'src', 'renderer', 'scenes', dirName
        ));

        // If the appPath exists and !overwrite -> exit
        if (
            fs.existsSync(path.normalize(path.join(appPath, 'index.js'))) &&
            overwrite !== true
        ) {
            return {error: `Component ${id} already exists`};
        }


        // Copy the template to the orc-ui scenes directory
        if (test === false) {
            fs.ensureDirSync(appPath);
            fs.copySync(templatePath, appPath);
        } else {
            testOutput['template-copy'] = { templatePath, appPath };
        }

        // Rename boiler plat files and update their contents
        const files = {
            'index.js'       : 'index.js',
            'sceneClass.jsx' : `${dirName}Class.jsx`,
            'style.scss'     : `${dirName}.scss`,
        };

        Object.entries(files).forEach(([oldFile, newFile], index) => {
            const oldFilePath = path.normalize(path.join(
                appPath,
                oldFile
            ));

            const newFilePath = path.normalize(path.join(
                appPath,
                newFile
            ));

            if (test === false) {
                fs.renameSync(oldFilePath, newFilePath);
            } else {
                testOutput[`boiler-copy-${index}`] = {
                    oldFilePath,
                    newFilePath
                };
            }

            // Update the boiler plate
            let content = fs.readFileSync(newFilePath, 'utf-8');
                content = content.replace(/\[ID\]/gi, id);
                content = content.replace(/\[DIR\]/gi, dirName);
                content = content.replace(/\[ICON\]/gi, icon);

            if (test === false) {
                fs.writeFileSync(newFilePath, content);
            } else {
                testOutput[`boiler-content-${index}-${newFile}`] = content;
            }
        });

        if (test !== false) {
            return testOutput;
        }
    },

    constants: ({ params, props }) => {
        // Get the source file path
        const sourceFilePath = path.normalize(path.join(
            props.cwd, 'src', 'renderer', 'redux', 'modules', 'router', 'constants.js'
        ));

        // Get the source file contents
        const sourceFileContents = fs.readFileSync(sourceFilePath, 'utf-8');

        // Clone file contents
        let output = String(sourceFileContents).substr(0);

        const { constant, constantValue } = params;

        // Do some light validation
        if (!constant || !constantValue) {
            return {error: `Constant: missing parameters`};
        }

        if (sourceFileContents.includes(` ${constant},`)) {
            return {error: `Constant ${constant} already exists`};
        }

        const replacers = [
            {
                regex   : new RegExp('\n\n\/\/ Add', 'smi'),
                replace : `export const ${constant} = '${constantValue}';\n\n\n// Add`,
            },
            {
                regex   : new RegExp('^export default {(.*)}', 'smi'),
                replace : `export default {$1  ${constant},\n}`
            }
        ];

        replacers.forEach((item) => {
            output = output.replace(item.regex, item.replace);
        });

        return {
            destination: {
                backup: path.normalize(path.join(
                    props.root, '.BACKUP', 'src', 'renderer', 'redux', 'modules', 'router', 'constants.js'
                )),
                update: sourceFilePath,
            },
            newFileContents: output,
            oldFileContents: sourceFileContents,
        }
    },

    routes: ({ params, props }) => {

        // Get the source file contents
        const sourceFilePath = path.normalize(path.join(
            props.cwd, 'src', 'renderer', 'scenes', 'routes.js'
        ));

        const sourceFileContents = fs.readFileSync(sourceFilePath, 'utf-8');

        // Clone file contents
        let output = String(sourceFileContents).substr(0);

        const { constant, route, icon } = params;

        // Do some light validation
        if (!constant || !route) {
            return {error: `Route missing parameters`};
        }

        if (sourceFileContents.includes(` ${constant},`)) {
            return {error: `Route ${constant} already exists`};
        }

        const obj = { constant, route, icon };

        const replacers = [
            {
                regex   : new RegExp('^} from', 'm'),
                replace : `  ${constant},\n} from`,
            },
            {
                regex : new RegExp('^];', 'm'),
                replace: '  '+JSON.stringify(obj)
                    .replace('"constant":', 'name: ')
                    .replace('"route":', 'path: ')
                    .replace('"icon":', 'icon: ')
                    .replace(/\"/gm, "'")
                    .replace(/{/, '{ ')
                    .replace(/}/, ' }')
                    .replace(/,/gm, ', ')
                    .replace(new RegExp(`'${constant}'`, 'm'), constant)
                    +',\n];'
            }
        ];

        replacers.forEach((item) => {
            output = output.replace(item.regex, item.replace);
        });

        return {
            destination: {
                backup: path.normalize(path.join(
                    props.root, '.BACKUP', 'src', 'renderer', 'scenes', 'routes.js'
                )),
                update: sourceFilePath,
            },
            newFileContents: output,
            oldFileContents: sourceFileContents,
        };
    },

    scenes: ({ params, props }) => {
        // Get the source file contents
        const sourceFilePath = path.normalize(path.join(
            props.cwd, 'src', 'renderer', 'scenes', 'index.jsx'
        ));

        const sourceFileContents = fs.readFileSync(sourceFilePath, 'utf-8');

        // Clone file contents
        let output = String(sourceFileContents).substr(0);

        const { constant, path:componentPath, id } = params;

        // Do some light validation
        if (
            !id ||
            !constant ||
            !componentPath
        ) { return {error: `Scene missing parameters`}; }

        if (sourceFileContents.includes(` ${constant},`)) {
            return {error: `Scene ${constant} already exists`};
        }

        const replacers = [
            {
                regex   : new RegExp('^} from', 'm'),
                replace : `  ${constant},\n} from`,
            },
            {
                regex   : new RegExp('(^\nexport default {$)', 'mi'),
                replace : `import ${id} from '.${componentPath}';\n$1`
            },
            {
                regex   : new RegExp('^export default {(.*)}', 'smi'),
                replace : `export default {$1  [${constant}]: ${id},\n}`
            }
        ];

        replacers.forEach((item) => {
            output = output.replace(item.regex, item.replace);
        });

        return {
            destination: {
                backup: path.normalize(path.join(
                    props.root, '.BACKUP', 'src', 'renderer', 'scenes', 'index.jsx'
                )),
                update: sourceFilePath,
            },
            newFileContents: output,
            oldFileContents: sourceFileContents,
        }
    },
};

const backup = ({ filePath, contents }) => {
    fs.ensureFileSync(filePath);
    fs.writeFileSync(filePath, contents);
};

const update = ({ filePath, contents }) => {
    fs.ensureFileSync(filePath);
    fs.writeFileSync(filePath, contents);
};

const Generator = ({ params, props, action }) => {

    if (action && action !== 'test') {
        return [actions[action]({ params, props, test: true })];
    } else {
        let errors = [];
        let result = Object.values(actions).map(gen => gen({ params, props, action }));

        result.forEach((item) => {
            if (!item) { return; }
            if (op.has(item, 'error')) {
                errors.push(item.error);
                return;
            }

            if (action !== 'test') {
                backup({
                    filePath : item.destination.backup,
                    contents : item.oldFileContents
                });

                update({
                    filePath : item.destination.update,
                    contents : item.newFileContents,
                });
            }
        });

        return { result, errors };
    }
};


module.exports = Generator;
