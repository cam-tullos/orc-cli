/**
 * -----------------------------------------------------------------------------
 * Imports
 * -----------------------------------------------------------------------------
 */

const beautify  = require('js-beautify');
const camelCase = require('camelcase');
const chalk     = require('chalk');
const slugify   = require('slugify');
const generator = require('./generator');

const {
    error,
    message
} = require('../../lib/messenger');

/**
 * conform(input:Object) Function
 * @description Reduces the input object.
 * @param input Object The key value pairs to reduce.
 * @since 0.0.1
 */
const CONFORM = (input) => {

    let output = {};

    Object.entries(input).forEach(([key, val]) => {
        let regex;
        let replacement;
        let str;

        switch (key) {
            case 'id':
                str = String(val).substr(0);

                regex = /\s/g;
                replacement = '_';
                str = str.replace(regex, replacement);

                regex = /[^\w]|_/g;
                replacement = '';

                output[key] = camelCase(
                    str.replace(regex, replacement),
                    { pascalCase: true }
                );
                break;

            case 'constant':
                str = String(val).substr(0);

                regex = /\s/g;
                replacement = '_';
                str = str.replace(regex, replacement);

                regex = /[^\w]/g;
                replacement = '_';
                str = str.replace(regex).toUpperCase();

                regex = /\.+/g;
                replacement = '.';
                output[key] = str.replace(regex, replacement);
                break;

            case 'constantValue':
                regex = /[^\w]|_/g;
                replacement = '.';
                str = String(val).substr(0);
                output[key] = str.replace(regex, replacement);
                break;

            case 'route':
                str = String(val).substr(0);
                output[key] = slugify(str, {
                    remove: /[*+~.()'"!@]/g
                });
                break;

            case 'icon':
                regex = /[^\w-]/g;
                replacement = '';
                str = String(val).substr(0);
                output[key] = str.replace(regex, replacement);
                break;

            case 'path':
                regex = /[^\w-\.\/]/g
                str = String(val).substr(0);
                output[key] = str.replace(regex, replacement);
                break;

            default:
                output[key] = val;
                break;
        }
    });

    return output;
};

/**
 * NAME String
 * @description Constant defined as the command name. Value passed to the commander.command() function.
 * @example $ orcli create:scene --some-flag
 * @see https://www.npmjs.com/package/commander#command-specific-options
 * @since 0.0.1
 */
const NAME = 'create:scene';


/**
 * DESC String
 * @description Constant defined as the command description. Value passed to
 * the commander.desc() function. This string is also used in the --help flag output.
 * @see https://www.npmjs.com/package/commander#automated---help
 * @since 0.0.1
 */
const DESC = 'Create a new Orc UI scene';


/**
 * HELP Function
 * @description Function called in the commander.on('--help', callback) callback.
 * @see https://www.npmjs.com/package/commander#automated---help
 * @since 0.0.1
 */
const HELP = () => {
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  None');
    console.log('');
};


/**
 * SCHEMA Object
 * @description used to describe the input for the prompt function.
 * @see https://www.npmjs.com/package/prompt
 * @since 0.0.1
 */
const SCHEMA = {
    properties: {
        id: {
            description: chalk.white('ID:'),
            required: true,
            message: 'ID is required and must be of type {String} [a-zA-Z0-9] and contain no spaces or special characters. Example: MY_NEW_SCENE',
            type: 'string',
        },
        constant: {
            description: chalk.white('Constant ID:'),
            required: true,
            message: 'Constant ID is required and must be of type {String}. Example: ',
            type: 'string',
        },
        constantValue: {
            description: chalk.white('Constant Value:'),
            required: true,
            message: 'Constant Value is required and must be of type {String}',
            type: 'string',
        },
        route: {
            description: chalk.white('Route:'),
            required: true,
            message: 'Route is a required and must be of type {String}. Example: /my/awesome/:scene',
            type: 'string',
        },
        icon: {
            description: chalk.white('Icon:'),
            type: 'string',
        },
        path: {
            description: chalk.white('Scene Directory'),
            message: 'Scene Directory is a required parameter',
            type: 'string',
        }
    }
};


/**
 * ACTION Function
 * @description Function used as the commander.action() callback.
 * @see https://www.npmjs.com/package/commander
 * @param opt Object The commander options passed into the function.
 * @param props Object The CLI props passed from the calling class `orcli.js`.
 * @since 0.0.1
 */
const ACTION = ({ opt, props }) => {
    const { prompt } = props;

    const ovr = {};
    Object.keys(SCHEMA.properties).forEach((key) => {
        if (opt[key]) { ovr[key] = opt[key]; }
    });

    prompt.override = ovr;
    prompt.start();
    prompt.get(SCHEMA, (err, input) => {
        // Keep this conditional as the first line in this function.
        // Why? because you will get a js error if you try to set or use anything related to the input object.
        if (err) {
            prompt.stop();
            error(`${NAME} cancelled`);
            return;
        }

        message(NAME, 'A scene will be created with the following data:');

        const params = CONFORM(input);

        console.log(chalk.cyan(beautify(
            JSON.stringify(params),
            { format: 'json' }
        )));
        console.log('');

        prompt.get({
            properties: {
                confirm: {
                    description: chalk.white('Proceed? (Y/N)'),
                    type: 'string',
                    required: true,
                    before: (val) => val.substr(0, 1).toLowerCase(),
                    pattern: /^y|n|Y|N/
                }
            }
        }, (err, result) => {
            if (err || result['confirm'] === 'n') {
                prompt.stop();
                message(NAME, 'cancelled');
                return;
            }

            const { confirm } = result;

            try {
                generator({ params, props });
                message(NAME, 'complete!');
            }
            catch (err) {
                error(`${NAME}`, err);
            }
        });
    });
};


/**
 * Module Constructor
 * @description Internal constructor of the module that is being exported.
 * @param program Class Commander.program reference.
 * @param props Object The CLI props passed from the calling class `orcli.js`.
 * @since 0.0.1
 */
const Create = (program, props) => {
    return {
        ACTION,
        COMMAND: program.command(NAME)
            .description(DESC)
            .action((opt) => { ACTION({ opt, props }); })
            .option('--id [id]', 'The unique id of the scene. Used as the component constructor.')
            .option('--constant [constant]', 'A unique constant used to reference the scene. Used in the constants.js declarations.')
            .option('--constant-value [constantValue]', 'The value corresponding to --constant. Used in the constants.js declarations.')
            .option('--route [route]', 'The url of the scene. Used in the route.js declarations.')
            .option('--icon [icon]', 'The sceneHeader icon.')
            .option('--path [path]', 'The destination path of the scene files relative inside the /src/renderer/scenes directory. Used if you want to structure your scene as a domain drive component.')
            .option('--overwrite [overwrite]', 'Replace the existing React Component for the scene.')
            .on('--help', HELP),
        CONFORM,
    }
};


/**
 * -----------------------------------------------------------------------------
 * Exports
 * -----------------------------------------------------------------------------
 */
module.exports = Create;
