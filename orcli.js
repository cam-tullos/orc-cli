#!/usr/bin/env node

'use strict';

// Imports
const config      = require(__dirname + "/config.json");
const ver         = require('./package').version;
const chalk       = require('chalk');
const path        = require('path');
const program     = require('commander');
const prompt      = require('prompt');
const cwd         = path.resolve(process.cwd());

// Build the props object
const props     = { cwd, root: __dirname, prompt, config };

// Configure prompt
prompt.message   = chalk[config.prompt.prefixColor](config.prompt.prefix);
prompt.delimiter = config.prompt.delimiter;

// Initialize the CLI
program.version(ver, '-v, --version');

// Apply commands
const commands = config.commands || [];
commands.forEach((cmd) => { require(cmd)(program, props).COMMAND; });


// Start the CLI
program.parse(process.argv);

// Output the help if nothing is passed
if (!process.argv.slice(2).length) {
    program.help();
}

module.exports = {
    props,
}
